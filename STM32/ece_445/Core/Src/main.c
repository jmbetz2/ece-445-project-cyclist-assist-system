/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
uint8_t farFlag = 0;
uint8_t midFlag = 0;
uint8_t cloFlag = 0;
uint8_t blFlag = 0;
uint8_t brFlag = 0;
uint8_t buzzerFlag = 0;

int farCount = 0;
int midCount = 0;
int cloCount = 0;
int blCount = 0;
int brCount = 0;

int farOn = 0;
int midOn = 0;
int cloOn = 0;
int blOn = 0;
int brOn = 0;

uint8_t prevFar = 0;
uint8_t prevMid = 0;
uint8_t prevClo = 0;
uint8_t prevBl = 0;
uint8_t prevBr = 0;

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
/* USER CODE BEGIN PFP */
void piRead();
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  piRead();
	  if(farFlag == 1 && midFlag != 1 && cloFlag != 1){
		  farCount++;
	  } else {
		  farCount = 0;
	  }
	  if(midFlag == 1 && cloFlag != 1){
		  midCount++;
	  } else {
		  midCount = 0;
	  }
	  if(cloFlag == 1){
		  cloCount++;
	  } else {
		  cloCount = 0;
	  }
	  if(blFlag == 1){
		  blCount++;
	  } else {
		  blCount = 0;
	  }
	  if(brFlag == 1){
		  brCount++;
	  } else {
		  brCount = 0;
	  }

	  if(farCount >= 10){
		  farOn = 50;
	  }
	  if(midCount >= 10){
		  midOn = 50;
	  }
	  if(cloCount >= 10){
		  cloOn = 50;
	  }
	  if(blCount >= 10){
		  blOn = 50;
	  }
	  if(brCount >= 10){
		  brOn = 50;
	  }

	  if(farOn > 0 && midOn == 0 && cloOn == 0){
		  HAL_GPIO_WritePin(GPIOA, LED_FAR_Pin, GPIO_PIN_SET);
		  farOn--;
	  } else {
		  HAL_GPIO_WritePin(GPIOA, LED_FAR_Pin, GPIO_PIN_RESET);
	  }
	  if(midOn > 0 && cloOn == 0){
		  HAL_GPIO_WritePin(GPIOA, LED_MID_Pin, GPIO_PIN_SET);
		  midOn--;
		  farOn = 0;
	  } else {
		  HAL_GPIO_WritePin(GPIOA, LED_MID_Pin, GPIO_PIN_RESET);
	  }
	  if(cloOn > 0){
		  HAL_GPIO_WritePin(GPIOA, LED_CLO_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOB, BUZZER_Pin, GPIO_PIN_SET);
		  cloOn--;
		  farOn = 0;
		  midOn = 0;
	  } else if (brOn == 0 && blOn == 0){//(brFlag < 10 && blFlag < 10){
		  HAL_GPIO_WritePin(GPIOA, LED_CLO_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(GPIOB, BUZZER_Pin, GPIO_PIN_RESET);
	  } else {
		  HAL_GPIO_WritePin(GPIOA, LED_CLO_Pin, GPIO_PIN_RESET);
	  }
	  if(blOn > 0){
		  HAL_GPIO_WritePin(GPIOB, LED_BL_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOB, BUZZER_Pin, GPIO_PIN_SET);
		  blOn--;
	  } else if (brFlag == 0 && cloFlag == 0){
		  HAL_GPIO_WritePin(GPIOB, LED_BL_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(GPIOB, BUZZER_Pin, GPIO_PIN_RESET);
	  } else {
		  HAL_GPIO_WritePin(GPIOB, LED_BL_Pin, GPIO_PIN_RESET);
	  }
	  if(brOn > 0){
		  HAL_GPIO_WritePin(GPIOB, LED_BR_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOB, BUZZER_Pin, GPIO_PIN_SET);
		  brOn--;
	  } else if (blOn == 0 && cloOn == 0){
		  HAL_GPIO_WritePin(GPIOB, LED_BR_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(GPIOB, BUZZER_Pin, GPIO_PIN_RESET);
	  } else {
		  HAL_GPIO_WritePin(GPIOB, LED_BR_Pin, GPIO_PIN_RESET);
	  }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_AFIO_CLK_ENABLE();
  __HAL_AFIO_REMAP_SWJ_NONJTRST();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LED_CLO_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOA, LED_FAR_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOA, LED_CLO_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED_BR_Pin|LED_BL_Pin|BUZZER_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : GPIO_3_Pin GPIO_4_Pin GPIO_14_Pin GPIO_15_Pin
                           GPIO_18_Pin */
  GPIO_InitStruct.Pin = GPIO_3_Pin|GPIO_4_Pin|GPIO_14_Pin|GPIO_15_Pin
                          |GPIO_18_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_CLO_Pin LED_MID_Pin LED_FAR_Pin */
  GPIO_InitStruct.Pin = LED_CLO_Pin|LED_MID_Pin|LED_FAR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_BR_Pin LED_BL_Pin BUZZER_Pin */
  GPIO_InitStruct.Pin = LED_BR_Pin|LED_BL_Pin|BUZZER_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void piRead(){
	farFlag = (HAL_GPIO_ReadPin(GPIOA,GPIO_3_Pin));
	midFlag = (HAL_GPIO_ReadPin(GPIOA,GPIO_4_Pin));
	cloFlag = (HAL_GPIO_ReadPin(GPIOA,GPIO_14_Pin));
	blFlag = (HAL_GPIO_ReadPin(GPIOA,GPIO_15_Pin));
	brFlag = (HAL_GPIO_ReadPin(GPIOA,GPIO_18_Pin));
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
