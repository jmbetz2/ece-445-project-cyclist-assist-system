/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define GPIO_3_Pin GPIO_PIN_0
#define GPIO_3_GPIO_Port GPIOA
#define GPIO_4_Pin GPIO_PIN_1
#define GPIO_4_GPIO_Port GPIOA
#define GPIO_14_Pin GPIO_PIN_2
#define GPIO_14_GPIO_Port GPIOA
#define GPIO_15_Pin GPIO_PIN_3
#define GPIO_15_GPIO_Port GPIOA
#define GPIO_18_Pin GPIO_PIN_4
#define GPIO_18_GPIO_Port GPIOA
#define LED_CLO_Pin GPIO_PIN_5
#define LED_CLO_GPIO_Port GPIOA
#define LED_MID_Pin GPIO_PIN_6
#define LED_MID_GPIO_Port GPIOA
#define LED_FAR_Pin GPIO_PIN_7
#define LED_FAR_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define LED_BR_Pin GPIO_PIN_4
#define LED_BR_GPIO_Port GPIOB
#define LED_BL_Pin GPIO_PIN_5
#define LED_BL_GPIO_Port GPIOB
#define BUZZER_Pin GPIO_PIN_6
#define BUZZER_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
