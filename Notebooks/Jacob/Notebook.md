# Jacob Worklog

# 2022-09-06 Dashboard Design Work 
Today I worked on the custom dashboard design and hardware. There were initially a few requirements that I knew we needed to fullfill 
* Raspberry Pi for Object Detection 
* Some sort of custom microcontroller to connect to the warning LEDs and IMU 
* Low cost, 6-axis IMU that can get bicycle roll 
* Rear camera to connect to Raspberry Pi 

The Raspberry Pi was a fairly easy decision as the new Raspberry Pi 4 has been known to be great for computer vision projects. The biggest challenge was really figuring out what kind of RAM requirments we would need for the project. An online recommendation said to use the 4GB or 8GB model. For our specific need, we don't need a lot of FPS when using whatever object detection software. We will take whichever one comes in stock to purchase first. 

![](https://assets.raspberrypi.com/static/raspberry-pi-4-labelled-f5e5dcdf6a34223235f83261fa42d1e8.png)

# 2022-09-14 STM32 and Raspberry Pi Purchase
A quick notebook update. Today I was able to purchase both the Raspberry Pi 4 8GB version and our STM32F103 series MCU. Both were at MSRP prices as well, hoping our hard to find hardware is done being found!

# 2022-09-16 Bicycle Measurement
Today I met Trisha at her apartment to take the needed measurments of her bicycle. There were several important things to measure:
* The distance of front handle bars
* The distance from the front handle bars, down the bike frame, and to the rear of the bike
* The rear of the bike measurements for the rear camera mount

The measurements I took can be seen in the image below. All of this information will be used to design the dashboard display enclosure soon!

![](bicycle_measurements.JPG)

# 2022-09-19 Dashboard CAD Design Work 
I worked for several hours to design the following dashboard enclosure in Fusion 360.

Overall, the design is fairly straightforward. Some things about the design:
* The design is roughly 8x5 inches, which should fit perfectly on Trisha's Bicycle right in the middle of the handle bars
* The design includes a shroud for the USB cable for the rear camera 
* There is a bottom enclosed section that will have a slot to slide the battery in and out of the dashboard if needed
* There is a cutout for the microusb charging port to charge the battery
* The design features 5mm light lenses of different colors so that the lights are larger and easier to see 
* The display cannot sit directly on top of the Raspberry Pi since the PCB has to slide over the GPIO pins.
* The display will connect to the custom PCB through male headers mounted and will connect to the Pi's HDMI port using custom mini-HDMI to HDMI adapters 
* Lastly, there is a removable lid on top to access inside the dashboard 

Here is an image from the CAD design of the dashboard

![](dashboard_design.JPG)

# 2022-09-20 Project Hardware Requirements 
Today I made a list of all of the hardware requirements we know that we need:
* Raspberry Pi 4 (8GB)
* STM32F103 MCU 
* Double Male Pin Headers
* 5V Boost Converter and Charge Circuit 
* 3.3V Linear Voltage Regulator 
* 3.7V LIPO Battery 
* 5mm Super Bright LED Lights
* HDMI Adapter
* HDMI Ribbon Cable 
* Mini HDMI Adapter 
* Resistors 
* Capacitors 
* MPU 6050 IMU 
* 3.5 inch HDMI Display
* USB 90 degree adapter 
* USB extension cable 

# 2022-09-25 Design Document Work and Circuit Schematic
I spent a lot of time today working on the design document and then getting started on the circuit schematic. The design document is pretty straightforward and a lot of the content can be taken from the project proposal. We did decide as a group that the IMU maybe should be dropped, so I made a new verison of our block diagram and our high level requirements so we can propose the idea to Jeff. Here is the new block diagram I helped make:
![](block_diagram.JPG)

Later I worked on the circuit schematic. I want to take over this part of the project (hardware, PCB, circuit, etc) since I have some previous experience in RSOs and because I am interested in it. Starting the circuit schematic was not too hard since we had the CAD assignment to help guide me. I added the STM32 using the data sheet and added the reset switch. I need to search for LDOs that are available as well before adding them.
![](first_circuit.JPG)
I will continue to work on the circuit this week so we can order the PCB soon!

# 2022-09-26 Design Document Work 
Today I talked with Jingdi and Trisha about how we will do object detection in OpenCV. I found an interesting article about using OpenCV with detecting motion. I am not sure its the avenue we want to go down, but still a good resource to use!https://towardsdatascience.com/image-analysis-for-beginners-creating-a-motion-detector-with-opencv-4ca6faba4b42

I also worked on the design document more. I focused on creating some flow charts for the STM32 software as well as finishing the requirement/verification part.

# 2022-09-27 Circuit Schematic Work
After talking to Jeff, he recommended finishing the circuit schematic and getting the PCB order out ASAP in case we find any problems. So since the design document work is getting wrapped up, I focused on the circuit schematic in KiCad. One thing I had to figure out today was what to use to drive the LEDs since the STM32 chip can only source 25ma and each of our LEDs are 20m. So we would literally run out of power if we had all the LEDs on at once. So I did some reasearch into the best option. I found two different ways: BJTs or MOSFETs. I have used BJTs before, but not MOSFETS. However, after doing some research and watching YouTube, N-Type MOSFETS make much more sense to me to use for our scenario since there is virtually no math needed and we can just source the gate of the MOSFET. I added the MOSFET to the circuit schematic, but haven't picked the exact one we will buy yet. I found an LDO that we can use (LD1117) that will supply power. Finally I added header pins for the programmer, Raspberry Pi and Display. So I think we have a first circuit iteration. I spent roughtly 5 hours finishing it today and I intend to start on the PCB design soon.
Here is an image of the first circuit: 
![](first_full_circuit.png)

# 2022-09-28 PCB Design V1
Today I worked on the inital PCB design in KiCad. Overall, it was fairly easy to route everything and get everything in place. There are a few traces that seem a little messy to me near some through-hole pins and around the STM32, but the rest seem to be okay. I think the most challenging part was placing each part in a good spot since the LEDs and display will be seen through the Lid of the dashboard. I use the measurment tool to roughtly measure everything and put it into the CAD model. The LEDs and display seem to be in an okay spot right now but they could change in the future. Another problem is that the PCB is massive and exceeds the course order size. We may just have to bite the bullet on this and buy on our own. 

Otherwise here are a few pictures showing the start and end process:
![](first_pcb_start.png)
![](first_pcb_done.png)
![](first_pcb_3d.png)

Later today, we have our design document check as well. 
# 2022-09-29 Final Design Document Changes and Part Selection 
We got some good feedback on our design document yesterday about things to change. We needed to update our schedule, some of our RV tables, and adjust some of the intro. Overall it was well recieved though! We did end up also adjusting the tolerance analysis to add some Raspberry Pi performance numbers since it is also involved in the power calculations. Here is some data we found: https://raspi.tv/2019/how-much-power-does-the-pi4b-use-power-measurements

# 2022-10-4 PCB Updates
Today we practiced for the design review as well as I worked on the the PCB to fix some of Jeffs comments. 

Jeff had some concerns about how close some traces were to through-hole pins as well as how messy the traces were around the STM32 chip. He made a good point that one bad solder could end up shorting something, so I had to get creative on how I would route the new traces. Initally, I was avoiding using vias and the back side, but I was forced to do so to clean up the PCB. The hardest change was the traces running from the RPi to the STM32/display. I had to run all the way around as you can see in the image below. It took me about 2 hours or so to fully fix it, but I do think it is close to being able to be ordered! 

![](second_pcb_done.png)

# 2022-10-5 Further PCB Updates
Today I spent even more time fixing the traces around the STM32 as well as adding some debugging through-holes for easy probing. You can see in the image from yesterday to today how much cleaner the PCB is around the STM32. Now there are no traces running on the inside of the MCU. The new probing spots are also on the edge of the board as well as right in front of the blind spot LEDs. I am very confident that this PCB is now ready to order since it passes all the design checks. Since we are so far ahead and we need a bigger PCB, we are gonna order from JLCPCB which has a 2-week recieve time!

![](third_pcb_done.png)

# 2022-10-7 Part selection and order 
My focus today was on ordering the needed parts for the project. Here is a list of everything we ordered today:
* Arducam Camera
* Battery 10Ah
* Mini Headsinks 
* Amp Ripper Boost Converter 

We also made a list on digikey of all the electronic components we would need for the PCB. However, before ordering I visited the ECE Supply Store and tried to get as many parts as they had in stock to fit our needs. Here is a list and picture of all the parts I was able to get 
* 15 Ohm Resistors
* 68 Ohm Resistors
* 82 Ohm Resistors
* Red LEDs
* Yellow LEDs
* Green LEDs
* Push Button Switches
* .1 uF Caps
* 1 uF Caps

![](supply_center.png)

Once we ordered all those parts, we put in an order at digikey with the rest of the resistors, capacitors, MOSFETS and pins we needed. They should hopefully be here soon. Tomorrow I am going to spend some time putting in the PCB order.

# 2022-10-9 PCB Order
My focus today was ordering the PCB. I spent a few hours figuring out how to generate the needed files to get the PCB made. It was actually pretty complicated at first since it wasn't working correctly on the JLCPCB website. I realized that I did not generate the gerber and drill files correctly, as they needed to be in a different format and zipped together. Once I fixed this, I was able to order the PCB from JLC and it was quickly approved for manufactoring. 

# 2022-10-10 Dashboard Enclosure V2
Since the PCB has been ordered and finalized I worked on making a second version of the fusion 360 model for the dashboard enclosure. 
Overall, the design is very similar to the original one with a few changes to the mounting and the lid itself. The screen is not really centered because of the PCB mounting, but its something we can overlook for the first prototype. Here are some pictures of the finished model: 

![](iso_dash2.png)
![](top_dash2.png)

# 2022-10-13 PCB and Parts Arrive 
Great news! The PCB and the parts from digikey both arrived today meaning that we can start the PCB soldering soon and will have plenty of time to solder it together and get it right. 

# 2022-10-16 PCB Soldering
I started soldering the PCB today, specifically focusing on the easy parts since it was my first time soldering a PCB. I started with resistors and capacitors since they are fairly large and they are very repeatitive. In about an hour I finished most of the resistors and capacitors but have been struggling with how large the soldering iron tip is compared to the solder pad. I feel like its taking too long per componenet, so I may need to talk to Jeff on Tuesday. 

# 2022-10-18 PCB Soldering
Today I continued soldering the PCB but first spoke with Jeff about soldering at our TA meeting. He pointed out that we could use the solder room in the back with the microscopes and it would be much easier since they have a finer tip. This greatly increased my soldering efficency and we were able to finish the last of the resistors and begin on the MOSFETs. The MOSFETS were very challenging and took several minutes and three hands since they were so small. Jingdi helped me hold them while I soldered them on. 

Overall, we have the made great process, but I am dreading doing the STM32 chip tomorrow since it looks like it will be very challenging. Luckily I have already watched some videos on the best way to solder it on so hopefully it helps.

# 2022-10-19 PCB Soldering Done
Today I spent a few hours finishing up the PCB soldering. I first tackled the STM32 chip which was much easier than I thought due to the flux saving the day. The chip itself only took about an hour to fully solder onto the board. The biggest challenge was a small short that I couldn't find for 20 minutes between the 3.3V pin and GND. Luckily I used some solder wick to remove it and there were no shorts on the board. The image below shows the PCB after the chip is on:

![](pcb_solder_stm.jpg)

The last thing I did before leaving the lab was solder on the LEDs and the buzzer. This only took like 30 minutes to do since they are all very easy solders. Now the PCB is done and is ready to be tested with the code. Hopefully the STM32 programs!

# 2022-10-22 STM32 Firmware Design 
My focus today was on setting up the firmware for the STM32 and to start desiging the code. The STM32CubeIDE is a great tool that allows you to set up a .ioc file which holds all the pinouts and settings for the microcontroller. All I really had to set up was all the GPIO pins for the RPi and MOSFET gates. The whole process only took about an hour and now the entire project has been pushed to GitLab. 

![](stm32_cube.png)

Later I worked on writing the actual loop code for controlling the LEDs. The code is pretty simple and only took me another hour to write. All it really does is use a while loop and constantly check the RPi GPIO for a high signal. If it flags a high signal it will turn on that LED with another GPIO high signal to the MOSFET. We should be able to test this code soon!

# 2022-10-25 PCB Testing Session 
Today's focus was on testing the PCB. After connecting our ST-Link to the PCB, we were able to program the chip meaning that the soldering actually worked! This was a huge relief for me since it was a challenging task. However, we quickly found a new problem when actually running the code on the PCB. For some reason the LEDs were always on, no matter a high or low signal from the GPIOs. After some probing for 2-3 hours, I found that the MOSFET itself was flipped. This was not due to a soldering error, rather the footprint in KiCad was flipped. This was a huge disapointment at first since this would cause a new PCB redesign and reorder. 

Even though we would need to reorder, I was able to adjust the PCB in about an hour using some wire and flipping the MOSFET itself on its back. I connected the wire from the GND pad to the other side of the MOSFET and then ran some wire from the LED to the drain. After this fix, we were able to actually test the PCB and its code! After adding some extra code for turning the LEDs off, we were able to fully test our working PCB! Even though we found a major problem and would need to reorder, we were able to see our PCB work. In the worst case scenario, we could still use this PCB in our final project as well if needed. Overall, still a sucessful day.

# 2022-10-27 PCB Fix 
After finding the PCB issue, today I got on KiCad and fixed the PCB's footprint issue. The issue was a quick fix and all I had to do was move the source/drain traces on each of the 6 MOSFETs in the schematic. While we were ordering a new PCB anyways, I added a few more "quality of life" updates to help us with our testing. These include:
* JST connector on the PCB for 5V connection 
* More probing spots for the RPi GPIO. 
* Power LED for when PCB is on 
* BOOT 0 and BOOT 1 headers incase of PCB memory failure 

All of these updates can be seen in the new schematic below:

![](third_pcb_3d.png) 

# 2022-10-28 Dashboard Print Begins
Today I went to print our dashboard lid and enclosure with some spare PLA filament I had. The lid was going to take about 8 hours to print and the enclosure is going to be a long print at a total of 2.5 days. They are going to do the Lid first, then the enclosure second. 

# 2022-10-31 Reorder PCB and Planning Session 
Today we made a list of all of the tasks we would need to do the next few weeks:

This week:  
- test display on PCB
- Pick up and assemble project case - Jacob 
- Test power subsystem - Jacob 
- Testing/select model - Trisha/Jingdi
- Distance measurement - Trisha/Jingdi
- Individual Report - all 
- Order new PCB - Jacob 

Next week 11/6-11/12:
- Testing object detection, rear view camera - all 
- Print camera mount - Jacob
- Attach on Bike (hose clamps) - Jacob 

Third week 11/13-11/19:
- Assemble on Bike - all
- Solder new PCB - Jacob/Jingdi 
- Test with movement - all
- Mock Demo - all 
- Begin Final Report - all 
- Prepare for Final Demo - all 

Thanksgiving Break:
- Final Paper - all 
- Extra Testing - all 
- Final Presentation - all
- Lab Notebook - all 

After this was done, I reordered the PCB on JLCPCB. I also requested that Trisha put in another digikey order with some extra parts for the new PCB order.

# 2022-11-1 RPi + PCB Integration 
Even though we reordered the PCB, we can still use the current PCB for system testing and verification. So today we focused on testing the RPi GPIO connection and the power subsystem. First the power subsystem was able to power everything with no detection running (RPi on idle) which was a great sign proving our calculations correct. We then went through our RV table for the power subsystem and verified we were hitting all requirements. 

After we ran the power subsystem, we tested the RPi GPIO connection. We just used jumper wires instead of soldering the RPi directly to the PCB since we were getting a new one. However, we were able to test the GPIO and turn on LEDs which was great! We did see some weird behavior with the Raspberry Pi floating GPIO pins, but this can hopefully be avoided in the STM32 code. 

![](full_system_test.jpg) 

It looks like Jingdi and Trisha will be focusing on the RPi software for the next few days, so our testing will be done for a bit. 

# 2022-11-6 Dashboard Encolsure Pickup and Assembly 
Today I picked up our 3D printed enclosure. They had to restart the enclosure because it failed about a day in, so the whole process took longer than expected. However, it is now in our hands so once we get the software done and the new PCB soldered we can put it on the bike. 

![](3d_dash_print.jpg) 

# 2022-11-11 PCB Soldering 
Today I picked up the new parts from the supply center and started soldering the updated PCB that just came in! I did the same order as last time, but this time I was much faster due to have soldering a PCB before. Today I achomplished soldering the resistors, capacitors, power LED, and all other SMD components other than the STM32 chip. 

# 2022-11-13 PCB Soldering 
Today I finished soldering the PCB. I started with the STM32 chip which was a little tougher this time since I accidently bent a pin and ripped a pad off. Luckily I didn't have to change anything since the solder pad was on a pin we were not using on the chip. Finally, I soldered the new LEDs and other through-hole components. This time I made sure to measure the LED height using the new lid and case so we could get it to be perfect height from cyclist perspective.

# 2022-11-14 First Project Test
Today we ran the entire project for the first time on the bike. First, I soldered the Raspberry Pi to the PCB so that the GPIO was directly connected with no jumper wires. After this we ran to the hardware store to get the needed screws to hold the PCB, RPi and Boost converter to the case. Sadly, several of the mounts broke on the case so we may need to redesign and reprint the case. However, we still got everything on the bike and ran it outside down the bike lane. All 3 of us rode it and found the performance to be very good for a first test. One issue we did find was with the battery dying quickly for no reason. However, I didn't charge it before testing so this could be why. 

![](final.png)

![](project.png)

# 2022-11-15 Mock Demo and Battery Debugging
Today we had our mock demo, but it was snowing outside so we didn't ride it down the bike lane with Jeff. Jeff told us to focus on getting the sensitivity down so Jingdi was going to think of some new ideas for the RPi code. 

After the demo, I spent some time debugging the battery. I found, using a multimeter in the lab, that the battery charge was fine but the interal BMS was dropping the battery voltage to 1V when the peak current got too high. Knowing that the Amp Ripper had an internal battery safety system, I soldered new connections to the battery cell itself and retested. This allowed the system to run as intended while still have overcurrent, overvoltage, and undervoltage protection through the Amp Ripper. This was great news as we now had a fully functioning project even at peak load. 

# 2022-11-16 Dashboard Redesign  
Today I focused on redesigning the dashboard since the mounts were broke on Monday and because the previous dashboard had a tight fit. Here is a list of the new things I added/fixed
* Adjusted new lid to fit dashboard screen better
* Quick swap battery slot 
* Larger bottom cavity for boost converter
* Internal middle cavity to route wires
* New PCB and RPi mounts (square and large)

![](fusion_dash_2.PNG)


# 2022-11-23 Dashboard Reprint and Repackaging
Over break I was able to reprint the new dashboard enclosure at my home 3D printer. The print now took over 3 days, but I was able to start as soon as I got home for break. Once the dashboard finished printing today, I spent a few hours removing supports, cleaning up the corners, and using bolts/nuts to attach all the boards to the enclosure. The final product was much cleaner and was going to mount on the bike much better. 

![](IMG-3405.jpg)

![](IMG-3408.jpg)

![](IMG-3406.jpg)

# 2022-11-28 Tuning and Testing
Today our focus was doing our final testing and tuning on the dashboard before our demo on Wednesday. We started off by attaching the new dashboard case to the bike and wiring everything together to run. We spent some time outside ECEB just riding around the bike lanes and testing performance. Then once we had visually verified everything, we started doing RV testing. We first use a Jingdi behind the bike with a tape measure to verify we were able to measure distance accurately. Then we used a car following to test on an actual vehicle. We found that our results for distance estimation were under 10% which was perfect for our requirement. We also tested in a very dark area to see if anything changed. 

Finally after verifing other requirements, we tried some new sensitivity code on the STM32 which greatly helped with the accuracy of cars in the mid and close distances. 

We also took a video since Jeff told us to take one before break. 
https://www.youtube.com/shorts/SWQN3_pFLPQ

# 2022-11-29 Final Tuning and Testing  
Tonight we did one final test of the project to be sure we weren't missing anything. Everything was working well and accurate. 

# 2022-11-30 Final Demonstration 
Demo went very well! Someone should have rode the bike though. 