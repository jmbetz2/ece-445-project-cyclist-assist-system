# 9.10
Today, I made a list of things we need to order for object detection subsystem:
* A camera with at least 720P, better to have night vision and wide view range
* Raspberry Pi 4B
* micro HDMI to HDMI converter DIY or buy one?
* 3.5'' LCD screen display for Raspberry Pi 4

# 9.15
I watched tutorials for object detections and openCV which will be used for image processing https://www.youtube.com/watch?v=0m387MkOyWw&t=692s&ab_channel=Digi-Key and a video related to model comparisons https://www.youtube.com/watch?v=twtBcfonSyE&list=PLQY2H8rRoyvz_anznBg6y3VhuSMcpN9oe&index=3. 

# 9.18
I learned basics about openCV, and how to capture frames and do conversions for BGR to RGB.
And I also looked through some hardwares and decided what we will need to order, and put them in our google list:
* Arducam USB Camera &nbsp; &nbsp; $49.99 &nbsp; &nbsp; how is this different from Pi cam?
* 3.5'' LCD Touch Screen Display &nbsp; &nbsp; $19.80
* DIY HDMI Cable Parts - Straight Mini HDMI Plug Adapter &nbsp; &nbsp; $6.50
* STM32F103 Microcontroller &nbsp; &nbsp; $24.00

# 9.23
Group decision made to drop the IMU + stability, and replace them with LED logics that features distance indication and blind spots warning using MOSFETs.

# 9.26
I designed the flow chart and the logics for flags to be sent from Raspberry Pi to STM32:
![](flowchart.png)

# 10.13
Today I tested the Arducam performance with Trisha. It's capable of streaming videos at 720P and even 1080P, and capable of night vision using IR filter. But we are still waiting for SD card to arrive so that we can start working on Raspberry Pi and the display. I also helped Jacob soldering PCB this week in the lab. We soldered and tested the circuits for MOSFETs, transisters, and the reset button.

# 10.25
We got STM32 programmed, but the MOSFETs are messed up due that the KiCAD pin indication for those were different from what we ordered. One solution we have for now is that we can turn MOSFETs to match the pins on PCB and solder and test them. And we will also reorder new PCB to make our board looks clean. 3D priting for our dashboard has began, hopefully we will get them soon and we will know if there are any changes we have to make when we try to mount parts.

# 10.28
Got the SD card, and I installed the Raspbian on our Raspberry Pi. `fswebcam -r 1280x720 --no-banner /image.jpg` were used to run and take a image using USB camera with 720P. Also installed a Efficientdet lite_0 model that was pre-trained on COCO 2017 dataset from tensorflow hub "https://tfhub.dev/s?deployment-format=lite&module-type=image-object-detection", optimized for edge devices. Achieved above 5.5fps, but the average precision was less than 35%. When I made it to only detect a few objects such as cars, people; it performed badly with many times recognizing phones as cars. But the night vision detection worked fine.
![](first_detection_test.jpg)

# 10.31
We made a list of what we will do for the next 3 weeks:  
This week:  
- test display on PCB
- Pick up and assemble project case - Jacob 
- Test power subsystem - Jacob 
- Testing/select model - Trisha/Jingdi
- Distance measurement - Trisha/Jingdi
- Individual Report - all 
- Order new PCB - Jacob 

Next week 11/6-11/12:
- Testing object detection, rear view camera - all 
- Print camera mount - Jacob
- Attach on Bike (hose clamps) - Jacob 

Third week 11/13-11/19:
- Assemble on Bike - all
- Solder new PCB - Jacob/Jingdi 
- Test with movement - all
- Mock Demo - all 
- Begin Final Report - all 
- Prepare for Final Demo - all 

Thanksgiving Break:
- Final Paper - all 
- Extra Testing - all 
- Final Presentation - all
- Lab Notebook - all 

# 11.1
I wrote up code for openCV capturing frames from USB camera, and had a loop structure to run the model. ![](mobilenet_test1.jpg)The updated model of our selection would be a mobilenet v2 SSD model that wes trained on BBD100K(Berkeley DeepDrive) dataset, which is a dataset intensionally to be used for on-road object detection! Huge improvement on accuracy while have above 4 fps! Average accuracy with 0.3 threshold was above 65% after tested on street by ECEB for 40 cars passed by. "https://bdd-data.berkeley.edu"  
The power subsystem tested and worked great with battery and boost converter. ![](power_subsystem.jpg)   
New PCB is ordered with MOSFETs pin locations corrected.

# 11.6
The 3D printed case arrived, the lower left edge of the cover doesn't fit well with the case, other parts seems to be fine and we are able to mount battery into its space.  
I start to write up codes for linear distance estimation. The idea is to use triangle similarity. The first thing is to figure out the focal length of our USB camera. I took a picure of a 11.7 inches width paper using our USB camera`F = (P x D) / W` was used for calculation. P is the perceived pixel width of the paper in the image. D is the known distance when I took the picture, and W is actual paper width which is 11.7 inches. The focal length F we found was 240. I used this focal length to estimate other objects using similarity equation again `D’ = (W’ x F) / P`, where D' is the estimated distance, W' is the known object width, P is the percieved pixel width of the object, and F is our calculated focal length. Me and Trisha decided to use W' = 70 inches for car because the average width of a car is 5.8 feet, and W' = 16 inches for pedestrians. 
![](Triangle_similarity.png)

# 11.11
Day to test distance estimation! We tried people standing 2m away from the camera, the estimation was 34 inches, and standing 5m away was 80 inches. The estimated distance was off. We found that it was because the picture we took for the paper wasn't in good lighting condition. We reason we need clear picture for the paper as marker is because the trick we were using to get the percieved pixel width from image is to extract contour line of the paper using openCV. Without good light condition, the extracted contour can be curved, or even missing some part. So we had to take another picture of our marker under better light condition. The new focal length from a better marker(paper) image was calculated to be 1180. We tested our estimation again and got 82 inches for 2m testing detecting people, and 205 inches for 5m testing detecting people. 

# 11.12
Tested distance estimation for cars. We had to adjust the W' for cars a little bit to 60 inches to get more precise distance estimation. Since the pre-trained model is more weighted on cars than on people, the detection and distance estimation are more accurate on cars which is great for our objectives of our project. I will start writing logics for GPIO output on Raspberry Pi tomorrow. These logics will decide when to set some GPIOs to high on Raspberry Pi depending on the estimated distance and whether they are located on the blind spots on cyclist. The way I plan to detect for blind spot is to check if the detected objects are located at the very left/right part of the image. 

# 11.13
Tested GPIOs aoutput on Raspberry Pi, didn't set everything to low as default and resulted in float. Fixed and tested again, Everything seems fine. The lights sometimes blinking fast, we might want to add some checking logics to make the lights not too sensitive. Plan is to have a counter for the detected object in the captured images, we don't light up the LED immediately when we detect it, but wait till it's detected for at least 3 or 5 consecutive images. This way, we kind of lift the threshold for lighting up LEDs. The other things we can do it to maintain the LEDs to be lighted up longer, instead of turning it off immediately when the object is no longer detected. 

# 11.14
We assembled the Raspberry Pi with dashboard to perform high level tests. The detection actually worked pretty good at night. I road the bike down bike lane and our system is capable of detecting cars behind me and those on my blind spot.
We might still want to adjust the sensitivity of our system because the detection model has false positive. One solution to deal with it is to have a counter to count the number of frames of this object being detected before light up the LEDs.
![](high_level_test.png)
