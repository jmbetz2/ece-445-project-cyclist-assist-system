# 9/10
* Did market research regarding other bike sensors on the market in order to determine the way for our product to be unique
* Debating the necessity of having an IMU sensor to detect roll stability in our project
* Did research into object detection with TensorFlow Lite
  * https://www.youtube.com/watch?v=0m387MkOyWw&t=692s&ab_channel=Digi-Key

# 9/18
* Met with Jingdi to decide on the camera and screen needed, and determined the power requirements for these two items
* Decided on the Arducam 1080P Day & Night Vision USB Camera for Computer for the rear camera
  * Power Supply: USB powered 5V
  * Working Current: MAX 370mA
  * Our requirements for the camera (project proposal) were that it has at least 720p resolution and a frame rate of 10 fps. This camera has 30fps at 1080P.
* Decided on 3.5'' LCD Touch Screen Display USB HDMI 1920x1080 RGB For Raspberry Pi 4 Model B for the front of bike user display
  * Our requirements for the camera were that it was 3.5 inches and that it has a quality of 720p or higher. This camera meets the length requirement and exceeds the quality requirements.

# 9/26
* Created flowchart for the Raspberry pi object detection and STM32 Software
* Researched OpenCV object detection and OpenCV distance detection
* Used this article to learn about distance detection with OpenCV: https://pyimagesearch.com/2015/01/19/find-distance-camera-objectmarker-using-python-opencv/
  * Learned about triangle similarity: using an object of known width, and putting it a known distance away. Let the camera determine the width in pixels. Then use the formula (Pixels*Distance)/Width to determine the focal length of the object
  * Then, input the width of the object you want to determine the distance from, and use the formula (Width*Focal Length)/Pixel Width to determine the distance from the camera to the object
  * Can use this to determine the distance between camera and cars, other vehicles, bikes, and people

# 10/4
* Met with whole group to review Design Document before Design review
* Reviewed PCB with TA to detect any errors and made adjustments

# 10/10
* Determined the components needed for this project, ordered through my.ece
![](myece.png)

# 10/13
* Tested the performance of the camera we will be using (Arducam) because it arrived this week
* Used our computer camera app to test the camera output
* Tested the night vision outside, it works well
* Works at 720P and 1080P
* We realized we would need an SD card to continue work on the raspberry pi, so i made an order for the SD/MicroSD Memory Card (8 GB SDHC) through my.ece
  * https://www.adafruit.com/product/1294

# 10/28
* We received the SD card from the supply center, so Jingdi and I worked on the OS for the raspberry pi, Raspbian
* Set up an object detection library that can detect any basic objects (keyboards, mouses, people, etc)
  * From: https://tfhub.dev/s?deployment-format=lite&module-type=image-object-detection
* Played around with only allowing certain objects to be shown
* Worked well at night, the object detection is a little inaccurate though

# 10/31
* Worked on a schedule to make sure the project stays on track

# 11/1
* The power subsystem tested and worked great with the battery and boost converter. 
* New PCB is ordered with MOSFETs pin locations corrected.
* Debated differences between Lite0 to Lite4 (one is larger/slower but more accurate, other is faster but less accurate)
* EfficientNet vs. MobileNet vs. Yolo
* Settled on the model Lite
* Explored TensorFlow Hub to look through trained machine learning models using EfficientNet
  * Tested accuracy and frame rate of different models
    * EfficientDet Lite3 is accurate but extremely extremely slow (unusable)
    * About 4 times slower than Lite0
* MobileObjectLocalizer is another ML model that cannot identify objects as what they are, but can find moving objects
  * Not sure if this is usable, because all objects will be considered mobile while a bike is moving
* Tested accuracy of object detection with different threshold levels
* Found a model that worked well for our purpose
  * https://medium.com/analytics-vidhya/autonomous-driving-object-detection-on-the-raspberry-pi-4-175bba51d5b4
  * This is MobileNet v2 SSD model
* High frame rate (>4fps), good accuracy
* Created another my.ece order for the new pcb (the same components ordered before, including a few new additions)

# 11/6
* Implemented the above model, but combined it with the EfficientDet Lite3 because the Berkeley model did not output a video stream, and instead had a series of photos saved
  * Below is a few screenshots of the detection outside
* The system has a good frame rate. It also does better when vehicles are approaching, rather than when vehicles are moving away
  * This works with our system because we only want to detect approaching vehicles
* Spent the rest of this meeting cleaning up the code for the object detection
  * Mainly removing the GPIO and LED prewritten code, since we will be recalibrating this ourselves
* Began researching distance detection using OpenCV - ran into a slight issue
  * The formula uses a standard width of an object to calculate the focal length and then calculate the distance away the object is
  * We can find the average width of a car to use for this calculation, but we need to find a way to distinguish between the width of a car/truck/person in our code
    * This is difficult with the current way objects are labeled in our code
![](object.png)
![](objectd.png)

# 11/8
* Began implementing distance estimation using triangle similarity that was previously researched
* We took a piece of paper (paper was used as the marker for triangle similarity in the example we based our work off of, this is because the width of paper is known) and set it a known distance from the camera
* We then converted it to grayscale and allowed our algorithm to calculate the focal length, and found that it was 240
  * It took several attempts to achieve this, because we had to find a place that had no other obstructing objects
* We used this to begin our distance detection algorithm
* We solved our previous issue about distinguishing between the different widths of the objects detected, and set different labels in our code to different known widths
  * Width of car: 70 inches, width of person: 16 inches
* These are average widths found online

# 11/11
* Tested our distance detection code on wright street outside ECEB
* Jingdi stood with sensor while I backed up 2m, 5m, and 10m away
* The distance detection was extremely incorrect, and we had to retune our focal length
  *Ran into the same issues as last time, as it took a while for us to get a picture that had no obstructions and had good lighting
* Got the new focal length: 1180
* This was a large difference from our original focal length, however it was a lot more accurate

# 11/12
* Today we went outside again but tested our algorithms accuracy with cars
* The distance detection algorithm was not as accurate, so our solution was to adjust the “known width” that we set for cars

# 11/14
* Set up our project to the bike and tested the accuracy while the bike was in motion
![](final.png)
![](project.png)